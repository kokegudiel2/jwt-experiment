test: 
	go test -v ./... -cover

run:
	go run main.go

ping:
	curl http://127.0.0.1:8484/ping -v
