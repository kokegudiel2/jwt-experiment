package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/kokegudiel2/jwt-experiment/database"
)

var passwordHash string

func TestHashPassword(t *testing.T) {
	user := User{
		Password: "Password",
	}

	err := user.HashPassword(user.Password)

	assert.NoError(t, err)

	passwordHash = user.Password
}

func TestCreateUserRecord(t *testing.T) {
	var user User

	err := database.InitDatabase()
	if err != nil {
		t.Error(err)
	}

	err = database.GlobalDb.AutoMigrate(&User{})
	assert.NoError(t, err)

	u := User{
		Name:     "Test User",
		Email:    "test@test.com",
		Password: passwordHash,
	}

	err = u.CreateUserRecord()
	assert.NoError(t, err)

	database.GlobalDb.Where("email = ?", u.Email).Find(&user)

	database.GlobalDb.Unscoped().Delete(&u)

	assert.Equal(t, "Test User", user.Name)
	assert.Equal(t, "test@test.com", user.Email)
}

func TestCheckPassword(t *testing.T) {
	u := User{Password: passwordHash}

	err := u.CheckPassword("Password")
	assert.NoError(t, err)
}
