package models

import (
	"log"

	"gitlab.com/kokegudiel2/jwt-experiment/database"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

//User model to map in db
type User struct {
	gorm.Model
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

// CreateUserRecord write user in db
func (u *User) CreateUserRecord() error {
	result := database.GlobalDb.Create(&u)
	if result.Error != nil {
		log.Println("Error: User record", result.Error)
		return result.Error
	}

	return nil
}

//HashPassword encrypt password using hash
func (u *User) HashPassword(password string) error {
	byte, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		log.Println("Error: Hashing", err)
		return err
	}

	u.Password = string(byte)

	return nil
}

//CheckPassword cheks user password
func (u *User) CheckPassword(password string) error {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
	if err != nil {
		log.Println("Error: Wrong Password", err)
		return err
	}

	return nil
}
