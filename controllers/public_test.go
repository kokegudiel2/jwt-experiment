package controllers

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/kokegudiel2/jwt-experiment/database"
	"gitlab.com/kokegudiel2/jwt-experiment/models"
)

func TestSignUp(t *testing.T) {
	var actualResult models.User

	user := models.User{
		Name:     "Test User",
		Email:    "test@test.com",
		Password: "password",
	}

	payload, err := json.Marshal(&user)
	assert.NoError(t, err)

	request, err := http.NewRequest("POST", "/api/public/signup", bytes.NewBuffer(payload))
	assert.NoError(t, err)

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = request

	err = database.InitDatabase()
	assert.NoError(t, err)

	database.GlobalDb.AutoMigrate(&models.User{})

	Signup(c)

	assert.Equal(t, http.StatusAccepted, w.Code)

	err = json.Unmarshal(w.Body.Bytes(), &actualResult)
	assert.NoError(t, err)

	assert.Equal(t, user.Name, actualResult.Name)
	assert.Equal(t, user.Email, actualResult.Email)
}

func TestSignUpInvalidJson(t *testing.T) {
	u := "test"

	payload, err := json.Marshal(&u)
	assert.NoError(t, err)

	request, err := http.NewRequest("POST", "/api/public/signup", bytes.NewBuffer(payload))
	assert.NoError(t, err)

	w := httptest.NewRecorder()

	c, _ := gin.CreateTestContext(w)
	c.Request = request

	Signup(c)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestLogin(t *testing.T) {
	user := LoginPayload{
		Email:    "test@test.com",
		Password: "secret",
	}

	payload, err := json.Marshal(&user)
	assert.NoError(t, err)

	request, err := http.NewRequest("POST", "/api/public/login", bytes.NewBuffer(payload))
	assert.NoError(t, err)

	w := httptest.NewRecorder()

	c, _ := gin.CreateTestContext(w)
	c.Request = request

	err = database.InitDatabase()
	assert.NoError(t, err)

	database.GlobalDb.AutoMigrate(&models.User{})

	Login(c)

	assert.Equal(t, http.StatusOK, w.Code)
}

func TestLoginInvalidJson(t *testing.T) {
	user := "test"

	payload, err := json.Marshal(&user)
	assert.NoError(t, err)

	request, err := http.NewRequest("POST", "/api/public/login", bytes.NewBuffer(payload))
	assert.NoError(t, err)

	w := httptest.NewRecorder()

	c, _ := gin.CreateTestContext(w)
	c.Request = request

	Login(c)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestLoginInvalidCredentials(t *testing.T) {
	user := models.User{
		Email:    "test@test.com",
		Password: "invalid",
	}

	payload, err := json.Marshal(&user)
	assert.NoError(t, err)

	request, err := http.NewRequest("POST", "/api/public/login", bytes.NewBuffer(payload))
	assert.NoError(t, err)

	w := httptest.NewRecorder()

	c, _ := gin.CreateTestContext(w)
	c.Request = request

	err = database.InitDatabase()
	assert.NoError(t, err)

	database.GlobalDb.AutoMigrate(&models.User{})

	Login(c)

	assert.Equal(t, http.StatusUnauthorized, w.Code)
}
