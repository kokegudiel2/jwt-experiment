package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/kokegudiel2/jwt-experiment/database"
	"gitlab.com/kokegudiel2/jwt-experiment/models"
)

func TestProfile(t *testing.T) {
	var profile models.User

	err := database.InitDatabase()
	assert.NoError(t, err)

	database.GlobalDb.AutoMigrate(&models.User{})

	user := models.User{
		Email:    "test@test.com",
		Password: "secret",
		Name:     "Test User",
	}

	err = user.HashPassword(user.Password)
	assert.NoError(t, err)

	err = user.CreateUserRecord()
	assert.NoError(t, err)

	request, err := http.NewRequest("GET", "/api/protected/profile", nil)
	assert.NoError(t, err)

	w := httptest.NewRecorder()

	c, _ := gin.CreateTestContext(w)
	c.Request = request

	c.Set("email", "test@test.com")

	Profile(c)

	err = json.Unmarshal(w.Body.Bytes(), &profile)
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, w.Code)

	log.Println(profile)

	assert.Equal(t, user.Email, profile.Email)
	assert.Equal(t, user.Name, profile.Name)
}

func TestLoginNotFound(t *testing.T) {
	var profile models.User

	err := database.InitDatabase()
	assert.NoError(t, err)

	database.GlobalDb.AutoMigrate(&models.User{})

	request, err := http.NewRequest("GET", "/api/protected/profile", nil)
	assert.NoError(t, err)

	w := httptest.NewRecorder()

	c, _ := gin.CreateTestContext(w)
	c.Request = request

	c.Set("email", "puto@puto.com")

	Profile(c)

	err = json.Unmarshal(w.Body.Bytes(), &profile)
	assert.NoError(t, err)

	assert.Equal(t, http.StatusNotFound, w.Code)

	database.GlobalDb.Unscoped().Where("email = ?", "test@test.com").Delete(&models.User{})
}
