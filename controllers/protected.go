package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/kokegudiel2/jwt-experiment/database"
	"gitlab.com/kokegudiel2/jwt-experiment/models"
	"gorm.io/gorm"
)

//Profile returns user data
func Profile(c *gin.Context) {
	var user models.User

	email, _ := c.Get("email")

	result := database.GlobalDb.Where("email = ?", email.(string)).First(&user)

	if result.Error == gorm.ErrRecordNotFound {
		c.JSON(http.StatusNotFound, gin.H{
			"msg": "user not found",
		})
		c.Abort()

		return
	}

	if result.Error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"msg": "Could not get user profile",
		})
		c.Abort()

		return
	}

	user.Password = ""

	c.JSON(http.StatusOK, user)

	return
}
