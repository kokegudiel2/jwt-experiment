package controllers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/kokegudiel2/jwt-experiment/auth"
	"gitlab.com/kokegudiel2/jwt-experiment/database"
	"gitlab.com/kokegudiel2/jwt-experiment/models"
	"gorm.io/gorm"
)

//Signup create a user in db
func Signup(c *gin.Context) {
	var user models.User

	err := c.ShouldBindJSON(&user)
	if err != nil {
		log.Println(err)

		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "Invalid json",
		})
		c.Abort()

		return
	}

	err = user.HashPassword(user.Password)
	if err != nil {
		log.Println(err)

		c.JSON(http.StatusInternalServerError, gin.H{
			"msg": "error hasing password",
		})
		c.Abort()

		return
	}

	err = user.CreateUserRecord()
	if err != nil {
		log.Println(err)

		c.JSON(http.StatusInternalServerError, gin.H{
			"msg": "error creating user",
		})
		c.Abort()

		return
	}

	c.JSON(http.StatusAccepted, user)
}

//LoginPayload login body
type LoginPayload struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

//LoginResponse token response
type LoginResponse struct {
	Token string `json:"token"`
}

//Login logs user in
func Login(c *gin.Context) {
	var payload LoginPayload
	var user models.User

	err := c.ShouldBindJSON(&payload)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "Invalid json",
		})
		c.Abort()

		return
	}

	result := database.GlobalDb.Where("email = ?", payload.Email).First(&user)
	if result.Error == gorm.ErrRecordNotFound {
		c.JSON(http.StatusUnauthorized, gin.H{
			"msg": "Invalid user credentials",
		})
		c.Abort()

		return
	}

	err = user.CheckPassword(payload.Password)
	if err != nil {
		log.Println(err)

		c.JSON(http.StatusUnauthorized, gin.H{
			"msg": "Invalid user credentials",
		})
		c.Abort()

		return
	}

	jwtWrapper := auth.JwtWrapper{
		SecretKey:       "veryscrecet",
		Issuer:          "AuthService",
		ExpirationHours: 24,
	}

	signedToken, err := jwtWrapper.GenerateToken(user.Email)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"msg": "error signing token",
		})
		c.Abort()

		return
	}

	tokenResponse := LoginResponse{
		Token: signedToken,
	}

	c.JSON(http.StatusOK, tokenResponse.Token)

	return
}
