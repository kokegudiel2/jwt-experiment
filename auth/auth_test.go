package auth

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var testToken string

func TestGenerateToken(t *testing.T) {
	jwtWrapper := JwtWrapper{
		SecretKey:       "secret",
		Issuer:          "AuthService",
		ExpirationHours: 24,
	}

	generatedToken, err := jwtWrapper.GenerateToken("test@test.com")
	assert.NoError(t, err)

	testToken = generatedToken
}

func TestValidateToken(t *testing.T) {
	encodedToken := testToken

	jwtWrapper := JwtWrapper{
		SecretKey: "secret",
		Issuer:    "AuthService",
	}

	claims, err := jwtWrapper.ValidateToken(encodedToken)
	assert.NoError(t, err)

	assert.Equal(t, "test@test.com", claims.Email)
	assert.Equal(t, "AuthService", claims.Issuer)
}
