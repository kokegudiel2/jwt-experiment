package database

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInitDb(t *testing.T) {
	err := InitDatabase()
	assert.NoError(t, err)
}
