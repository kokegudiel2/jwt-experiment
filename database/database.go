package database

import (
	"log"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

// GlobalDb db object will be used across diferent package
var GlobalDb *gorm.DB

func InitDatabase() (err error) {
	GlobalDb, err = gorm.Open(sqlite.Open("auth.db"), &gorm.Config{})
	if err != nil {
		log.Println("Error: Init DB", err)
		return
	}

	return nil
}
