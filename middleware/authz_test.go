package middleware

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/kokegudiel2/jwt-experiment/auth"
	"gitlab.com/kokegudiel2/jwt-experiment/controllers"
	"gitlab.com/kokegudiel2/jwt-experiment/database"
	"gitlab.com/kokegudiel2/jwt-experiment/models"
)

func TestAuthzNoHeader(t *testing.T) {
	router := gin.Default()
	router.Use(Authz())

	router.GET("/api/protected/profile", controllers.Profile)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/protected/profile", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusForbidden, w.Code)
}

func TestAuthzInvalidToken(t *testing.T) {
	invalidToken := "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
	router := gin.Default()
	router.Use(Authz())

	router.GET("/api/protected/profile", controllers.Profile)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/protected/profile", nil)
	req.Header.Add("Authorization", invalidToken)

	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusUnauthorized, w.Code)
}

func TestValidToken(t *testing.T) {
	var response models.User

	err := database.InitDatabase()
	assert.NoError(t, err)

	err = database.GlobalDb.AutoMigrate(&models.User{})
	assert.NoError(t, err)

	user := models.User{
		Email:    "test@test.com",
		Password: "secret",
		Name:     "Test User",
	}

	jwtWrapper := auth.JwtWrapper{
		SecretKey:       "secretkey",
		Issuer:          "AuthService",
		ExpirationHours: 24,
	}

	token, err := jwtWrapper.GenerateToken(user.Email)
	assert.NoError(t, err)

	router := gin.Default()
	router.Use(Authz())

	router.GET("/api/protected/profile", controllers.Profile)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/protected/profile", nil)
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))

	router.ServeHTTP(w, req)

	err = json.Unmarshal(w.Body.Bytes(), &response)
	assert.NoError(t, err)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "test@test.com", response.Email)
	assert.Equal(t, "Test User", response.Name)

	database.GlobalDb.Unscoped().Where("email = ?", user.Email).Delete(&models.User{})
}
