package middleware

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/kokegudiel2/jwt-experiment/auth"
)

//Authz validates token and authorizes users
func Authz() gin.HandlerFunc {
	return func(c *gin.Context) {
		clientToken := c.Request.Header.Get("Authorization")
		if clientToken == "" {
			c.JSON(http.StatusForbidden, "No Authorization header provided")
			c.Abort()

			return
		}

		extractedToken := strings.Split(clientToken, "Bearer ")

		if len(extractedToken) != 2 {
			c.JSON(http.StatusBadRequest, "Incorrect Format of Authorization Token")
			c.Abort()

			return
		}

		clientToken = strings.TrimSpace(extractedToken[1])

		jwtWrapper := auth.JwtWrapper{
			SecretKey: "secretekey",
			Issuer:    "AuthService",
		}

		claims, err := jwtWrapper.ValidateToken(clientToken)
		if err != nil {
			c.JSON(http.StatusUnauthorized, err.Error())
			c.Abort()

			return
		}

		c.Set("email", claims.Email)

		c.Next()
	}
}
