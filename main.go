package main

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/kokegudiel2/jwt-experiment/controllers"
	"gitlab.com/kokegudiel2/jwt-experiment/database"
	"gitlab.com/kokegudiel2/jwt-experiment/models"
)

func setupRouter() *gin.Engine {
	r := gin.Default()

	r.GET("/ping", func(ctx *gin.Context) {
		ctx.String(200, "pong")
	})

	api := r.Group("/api")
	{
		public := api.Group("/public")
		{
			public.POST("/login", controllers.Login)
			public.POST("/signup", controllers.Signup)
		}

		protected := api.Group("/protected")
		{
			protected.GET("/profile", controllers.Profile)
		}
	}

	return r
}

func main() {
	err := database.InitDatabase()
	if err != nil {
		log.Fatalln("could not create db", err)
	}

	database.GlobalDb.AutoMigrate(&models.User{})

	r := setupRouter()
	r.Run(":8484")
}
